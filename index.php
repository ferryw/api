<?php

$url = 'https://simple-contact-crud.herokuapp.com/contact'; // path to your JSON file
$data = file_get_contents($url); // put the contents of the file into a variable
$wizards = json_decode($data, true); // decode the JSON feed

$wizard = $wizards["data"];
//echo $wizard[0]["id"];

//var_dump($wizards["data"][0]["id"]);

//foreach ($wizard as $row) :
//	echo $row["id"] . ' @ ' . $row["firstName"] . ' @ ' . $row["lastName"] . ' @ ' . $row["age"] . ' @ ' . $row["photo"] . '<br>' ;
//endforeach;
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">         
  <div align="right" style="margin-bottom:5px;">
	<button type="button" name="add_button" id="add_button" class="btn btn-success btn-xs">Add</button>
  </div>
  <table class="table text-center">
    <thead>
      <tr>
        <th>Firstname</th>
        <th>Lastname</th>
        <th>Age</th>
		<th>Photo</th>
		<th col=2>Action</th>
      </tr>
    </thead>
    <tbody></tbody>
  </table>
</div>
</body>
</html>

<div id="apicrudModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<form method="post" id="api_crud_form">
				<div class="modal-header">
		        	<button type="button" class="close" data-dismiss="modal">&times;</button>
		        	<h4 class="modal-title">Add Data</h4>
		      	</div>
		      	<div class="modal-body">
		      		<div class="form-group">
			        	<label>First Name</label>
			        	<input type="text" name="first_name" id="first_name" class="form-control" />
			        </div>
			        <div class="form-group">
			        	<label>Last Name</label>
			        	<input type="text" name="last_name" id="last_name" class="form-control" />
			        </div>
					<div class="form-group">
			        	<label>Age</label>
			        	<input type="text" name="age" id="age" class="form-control" />
			        </div>
					<div class="form-group">
			        	<label>Photo</label>
			        	<div class="custom-file mb-3">
						  <input type="file" class="custom-file-input" id="customFile" name="filename">
						  <label class="custom-file-label" for="customFile">Choose file</label>
						</div>
			        </div>
			    </div>
			    <div class="modal-footer">
			    	<input type="hidden" name="hidden_id" id="hidden_id" />
			    	<input type="hidden" name="action" id="action" value="insert" />
			    	<input type="submit" name="button_action" id="button_action" class="btn btn-info" value="Insert" />
			    	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      		</div>
			</form>
		</div>
  	</div>
</div>


<script type="text/javascript">
$(document).ready(function(){

	fetch_data();

	function fetch_data()
	{
		$.ajax({
			url:"fetch.php",
			success:function(data)
			{
				$('tbody').html(data);
			}
		})
	}

	$('#add_button').click(function(){
		$('#action').val('insert');
		$('#button_action').val('Insert');
		$('.modal-title').text('Add Data');
		$('#apicrudModal').modal('show');
	});

	$('#api_crud_form').on('submit', function(event){
		event.preventDefault();
		if($('#first_name').val() == '')
		{
			alert("Enter First Name");
		}
		else if($('#last_name').val() == '')
		{
			alert("Enter Last Name");
		}
		else
		{
			var form_data = $(this).serialize();
			$.ajax({
				url:"action.php",
				method:"POST",
				data:form_data,
				success:function(data)
				{
					fetch_data();
					$('#api_crud_form')[0].reset();
					$('#apicrudModal').modal('hide');
					if(data == 'insert')
					{
						alert("Data Inserted using PHP API");
					}
					if(data == 'update')
					{
						alert("Data Updated using PHP API");
					}
				}
			});
		}
	});

	$(document).on('click', '.edit', function(){
		var id = $(this).attr('id');
		var action = 'fetch_single';
		$.ajax({
			url:"action.php",
			method:"POST",
			data:{id:id, action:action},
			dataType:"json",
			success:function(data)
			{
				$('#hidden_id').val(id);
				$('#firstName').val(data["firstName"]);
				$('#lastName').val(data.lastName);
				$('#action').val('update');
				$('#button_action').val('Update');
				$('.modal-title').text('Edit Data');
				$('#apicrudModal').modal('show');
			}
		})
	});

	$(document).on('click', '.delete', function(){
		var id = $(this).attr("id");
		var action = 'delete';
		if(confirm("Are you sure you want to remove this data using PHP API?"))
		{
			$.ajax({
				url:"action.php",
				method:"POST",
				data:{id:id, action:action},
				success:function(data)
				{
					fetch_data();
					alert("Data Deleted using PHP API");
				}
			});
		}
	});

});
</script>

