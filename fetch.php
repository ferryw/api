<?php

//fetch.php

$url = 'https://simple-contact-crud.herokuapp.com/contact'; // path to your JSON file
$data = file_get_contents($url); // put the contents of the file into a variable
$wizards = json_decode($data, true); // decode the JSON feed

$wizard = $wizards["data"];

$output = '';

if(count($wizard) > 0)
{
	foreach($wizard as $row)
	{
	if($row["photo"]=='' || $row["photo"]=='N/A'){$row["photo"]='';}else{ $row["photo"]='<img src="'.$row["photo"].'" width=100 />';}
		$output .= '
		<tr>
			<td>'.$row["firstName"].'</td>
			<td>'.$row["lastName"].'</td>
			<td>'.$row["age"].'</td>
			<td>'.$row["photo"].'</td>
			<td><button type="button" name="edit" class="btn btn-warning btn-xs edit" id="'.$row["id"].'">Edit</button></td>
			<td><button type="button" name="delete" class="btn btn-danger btn-xs delete" id="'.$row["id"].'">Delete</button></td>
		</tr>
		';
	}
}
else
{
	$output .= '
	<tr>
		<td colspan="4" align="center">No Data Found</td>
	</tr>
	';
}

echo $output;

?>